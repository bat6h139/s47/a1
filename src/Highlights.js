

export default function Highlights(){
    return(
        <div className="container-fluid">
            <div className="row mb-3">
                {/*Card 1 */}
                <div className="col-10 col-md-4">
                    <div class="card">
  
                        <div class="card-body">
                            <h5 class="card-title">Learn from home</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content. Some quick example text to build on the card title and make up the bulk of the card's content. Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                    </div>
                </div>
                {/*Card 2 */}
                <div className="col-10 col-md-4">
                    <div class="card">
  
                        <div class="card-body">
                            <h5 class="card-title">Be part of our Community</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content. Some quick example text to build on the card title and make up the bulk of the card's content. Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                    </div>
                </div>
                {/*Card 3 */}
                <div className="col-10 col-md-4">
                    <div class="card">
  
                        <div class="card-body">
                            <h5 class="card-title">Study Now Pay Later</h5>
                            <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content. Some quick example text to build on the card title and make up the bulk of the card's content. Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}