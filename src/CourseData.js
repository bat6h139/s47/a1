
const CourseData = [
    {
        id: "wdc001",
        name: "PHP - Laravel",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti rerum tenetur asperiores minus! Odit aliquam non veritatis harum necessitatibus doloribus, ab debitis pariatur illum sit officiis facilis animi numquam maxime!",
        price: 45000,
        onOffer: true
    },
    {
        id: "wdc002",
        name: "Java - Stringboot",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti rerum tenetur asperiores minus! Odit aliquam non veritatis harum necessitatibus doloribus, ab debitis pariatur illum sit officiis facilis animi numquam maxime!",
        price: 50000,
        onOffer: true
    },
    {
        id: "wdc003",
        name: "Node - Express",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti rerum tenetur asperiores minus! Odit aliquam non veritatis harum necessitatibus doloribus, ab debitis pariatur illum sit officiis facilis animi numquam maxime!",
        price: 55000,
        onOffer: true
    }
]

export default CourseData