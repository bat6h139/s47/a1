import { Container } from 'react-bootstrap';
import { BrowserRouter, Switch, Route } from 'react-router-dom/cjs/react-router-dom.min';
// import UserContext from './UserContext';
// import { useContext } from 'react';
import AppNavbar from './AppNavbar';
import { useContext, useEffect, useState } from 'react';

import Courses from './Pages/Courses';
import Error from './Pages/Error';
import Home from './Pages/Home';
import Login from './Pages/Login';
import Logout from './Pages/Logout';
import Register from './Pages/Register';
import UserContext from './UserContext';
import { Redirect } from 'react-router-dom';

export default function Routes(){

    const { user } = useContext(UserContext)
    console.log(user)

    const [loggedin, setLoggedin] = useState(true)

    useEffect( () => {
        if(user == null){
            setLoggedin(true)
        } else {
            setLoggedin(false)
        }
    }, [user])
        
    return(
        <BrowserRouter>
            <AppNavbar />
                <Container fluid className='m-3'>
                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route exact path="/courses" component={Courses}/>
                        <Route exact path="/login" component={Login}/>
                        <Route exact path="/logout" component={Logout}/>
                        <Route exact path={"/register"}  >
                            {loggedin ?<Redirect to={"courses"} />: <Register/> }
                        </Route>
                        <Route component={Error}/>
                        
                    </Switch>
                </Container>
    </BrowserRouter>
    )
}