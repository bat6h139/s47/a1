import  {Navbar, Nav} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import {NavLink } from 'react-router-dom/cjs/react-router-dom.min'
import { Fragment, useContext } from 'react';
import UserContext from './UserContext';


function AppNavbar() {

  const { user } = useContext(UserContext)
  //const [user] = useState(localStorage.getItem("email"))
  //console.log(user)


  let leftNav = (user.email != null) ? 
    <Nav.Link as={NavLink} to={'/logout'}>Logout</Nav.Link>
  :
    <Fragment>
      <Nav.Link as={NavLink} to={'/register'}>Register</Nav.Link>
      <Nav.Link as={NavLink} to={'/login'}>Login</Nav.Link> 
    </Fragment>

 

  return (
    <Navbar bg="primary" variant="dark">
    <Navbar.Brand href="#home">Course Booking</Navbar.Brand>
    <Nav className="me-auto">
      <Nav.Link as={NavLink} to={'/'}>Home</Nav.Link>
      <Nav.Link as={NavLink} to={'/courses'}>Courses</Nav.Link>
      

      {/* <Nav.Link>Register</Nav.Link>
      <Nav.Link>Login</Nav.Link>
      <Nav.Link>Logout</Nav.Link> */}
    </Nav >
    <Nav className="me-auto">
      {leftNav}  
    </Nav>
  </Navbar>
  );
}

export default AppNavbar;