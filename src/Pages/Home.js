import { Fragment } from "react";
import Banner from "../Banner";
import Highlights from "../Highlights";


export default function Home(){

    const data = {
        title: "Zuitt Coding Bootcam",
        content: "Opportunities for everyone, everywhere",
        destination: "/courses",
        label: "Enroll Now"
    }

    return(
        <Fragment>
            <Banner data={data}/>
            <Highlights />
        </Fragment>
    )
}