import {useState, useEffect} from 'react'
import { Container, Form, Button } from "react-bootstrap";

export default function Register(){

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [cpw , setCpw] = useState("")

    const [isDisabled, setIsDisabled] = useState(true)

    function Register(e){
        e.preventDefault()

        alert("Registered Successfully")
    }

    useEffect( () => {
		if(email !== "" && password !== "" && cpw !== ""){
            if(password === cpw){
                setIsDisabled(false)
            }
        } 
         else{
            setIsDisabled(true)
        }

	}, [email, password, cpw] )


    return (
        <Container >
            <Form className='border p-3 border'>
                <Form.Group className="mb-3" controlId="email">
                <Form.Label>Email address</Form.Label>
                < Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    />
                </Form.Group>

                <Form.Group className="mb-3" controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password} 
                    onChange={(e) => setPassword(e.target.value)}
                    />
                </Form.Group>
                <Form.Group className="mb-3" controlId="cpw">
                <Form.Label>Confirm Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value={cpw}
                    onChange={(e) => setCpw(e.target.value)}
                    />
                </Form.Group>
                
                <Button 
                    variant="primary" 
                    type="submit" 
                    disabled={isDisabled}
                    onClick={Register}
                    >
                 Submit
  </            Button>
                </Form>
        </Container>
    )
}