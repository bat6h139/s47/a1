
import CourseData from "../CourseData"
import CourseCard from '../CourseCard'
import { Fragment } from "react"

export default function Courses(){

    const courses = CourseData.map( element => {
		console.log(element)	//each object in the courseData array

		return(
			<CourseCard key={element.id} {...element}/>
		)
	})

    return (
        <Fragment>
            <div>
                {courses}
            </div>
            
        </Fragment>
    )
}