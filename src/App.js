
import 'bootstrap/dist/css/bootstrap.min.css'
import { useState } from 'react';
import Routes from './Routes';

// For React Context
import { UserProvider } from './UserContext';
//import { Fragment } from 'react';
// import { Container } from 'react-bootstrap';
// import { BrowserRouter, Switch, Route } from 'react-router-dom/cjs/react-router-dom.min';

// import AppNavbar from './AppNavbar';
// import Courses from './Pages/Courses';
// import Home from './Pages/Home';
// import Login from './Pages/Login';
// import Logout from './Pages/Logout';
// import Register from './Pages/Register';


function App() {
  /*React Context is nothing but a global state to the app. It is a way to make a particular data available to all the components no  matter how they are nested. Context helps you broadcast the data and changes happening to that data, to all components */

  const [user, setUser] = useState({
    email: localStorage.getItem('email')
  })
  const unsetUser = () => {
    localStorage.clear()
  }

  return (
      <UserProvider value={{user, setUser, unsetUser}}>
        <Routes/>
      </UserProvider>

  );
}

export default App;
